import React, { FunctionComponent, useContext } from 'react';
import { View, Text } from 'react-native';

import AppContext from '../../../src/contexts/AppContext';
import { formStyles } from '../../common/styles';
import { NavigationProps } from '../../types/navigation-props';
import firebase from '../../api-sdk/firebase';
import Button from '../../components/Button';

const SignUp: FunctionComponent<NavigationProps> = () => {
  const { lang, user } = useContext(AppContext);

  const handleLogout = () => firebase.auth().signOut();

  return (
    <View style={formStyles.container}>
      <Text style={[formStyles.cta, formStyles.ctaText]}>
        {lang!['app.user.profile']}
      </Text>
      {user && (
        <Text style={[formStyles.cta, formStyles.ctaText]}>
          {user.displayName}
        </Text>
      )}
      <Button
        title={lang!['app.buttons.logout']}
        btnStyles={formStyles.btnStyles}
        titleStyles={formStyles.titleStyles}
        onPress={handleLogout}
      />
    </View>
  );
};

export default SignUp;
