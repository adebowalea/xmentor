/*
 *  https://callstack.github.io/react-native-testing-library/docs/api/
 *  https://reactnavigation.org/docs/testing
 */
import React, { FunctionComponent } from 'react';
import { render } from '@testing-library/react-native';

import AppContext from '../../../../src/contexts/AppContext';
import en from '../../../common/translations/en.json';
import { mockNavigation } from '../../../utils/mock-navigation';
import SignInComponent from '../';

const SignInScreen: FunctionComponent = () => {
  return (
    <AppContext.Provider value={{ lang: en, user: null }}>
      <SignInComponent navigation={mockNavigation} />
    </AppContext.Provider>
  );
};

describe('<SignInScreen />: ', () => {
  it('--- should renders correctly', () => {
    const container = render(<SignInScreen />).toJSON();
    expect(container).toMatchSnapshot();
  });
});
