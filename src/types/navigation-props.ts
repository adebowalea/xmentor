/*
 *  https://reactnavigation.org/docs/typescript/
 *  https://reactnavigation.org/docs/navigation-context
 */
import { StackNavigationProp } from '@react-navigation/stack';

type RootStackParamList = {
  XMentor: undefined;
  'Sign in': undefined;
  'Sign up': undefined;
  Profile: undefined;
};

export enum Screens {
  XMentor = 'XMentor',
  SignIn = 'Sign in',
  SignUp = 'Sign up',
  Profile = 'Profile',
}

type ScreenNavigationProp = StackNavigationProp<RootStackParamList, Screens>;

export type NavigationProps = {
  navigation: ScreenNavigationProp;
};
