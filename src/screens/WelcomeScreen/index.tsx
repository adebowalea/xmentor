import React, { FunctionComponent, useContext } from 'react';
import { View, Text, ImageBackground } from 'react-native';

import Button from '../../components/Button';
import { NavigationProps, Screens } from '../../types/navigation-props';
import AppContext from '../../../src/contexts/AppContext';
import styles from './styles';

const WelcomeScreen: FunctionComponent<NavigationProps> = ({ navigation }) => {
  const { lang } = useContext(AppContext);
  const handlePress = (screen: Screens) => navigation.navigate(screen);

  return (
    <ImageBackground
      style={styles.background}
      source={require('../../common/assets/background.jpg')}
    >
      <View style={styles.logoBg} />
      <View style={styles.logoContainer}>
        <Text style={styles.tagline}>{lang!['app.name']}</Text>
      </View>
      <View style={styles.buttonsContainer}>
        <Button
          title={lang!['app.buttons.signin']}
          btnStyles={styles.signIn}
          titleStyles={styles.signInTitle}
          onPress={() => handlePress(Screens.SignIn)}
        />
        <Button
          title={lang!['app.buttons.signup']}
          btnStyles={styles.signUp}
          titleStyles={styles.signUpTitle}
          onPress={() => handlePress(Screens.SignUp)}
        />
      </View>
    </ImageBackground>
  );
};

export default WelcomeScreen;
