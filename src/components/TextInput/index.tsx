import React, { FunctionComponent } from 'react';
import { TextInput, StyleSheet, TextInputProps } from 'react-native';

import colors from '../../common/colors';

const CustomTextInput: FunctionComponent<TextInputProps> = ({
  placeholder,
  onChangeText,
  ...otherProps
}) => {
  return (
    <TextInput
      autoCapitalize="none"
      autoCorrect={false}
      onChangeText={onChangeText}
      placeholder={placeholder}
      style={styles.input}
      {...otherProps}
    />
  );
};

const styles = StyleSheet.create({
  input: {
    width: '80%',
    color: colors.black,
    borderColor: colors.lightGray,
    borderWidth: 1,
    padding: 15,
    borderRadius: 10,
    backgroundColor: colors.white,
    marginVertical: 10,
    fontSize: 18,
  },
});

export default CustomTextInput;
