import React, { FunctionComponent, useContext } from 'react';
import { View, Text } from 'react-native';

import Button from '../../components/Button';
import { formStyles } from '../../common/styles';
import { NavigationProps, Screens } from '../../types/navigation-props';
import TextInput from '../../components/TextInput';
import AppContext from '../../../src/contexts/AppContext';
import { api } from '../../api-sdk';
import useForm from '../../hooks/useForm';
import { LoginProps } from '../../types/forms';

const SignIn: FunctionComponent<NavigationProps> = ({ navigation }) => {
  const { lang } = useContext(AppContext);
  const { form, handleOnChange, handleSubmit } = useForm<Partial<LoginProps>>(
    {},
    api.login
  );

  const gotoProfile = () => navigation.navigate(Screens.Profile);

  return (
    <View style={formStyles.container}>
      <TextInput
        onChangeText={text => handleOnChange(text, 'email')}
        placeholder={lang!['app.signin.placeholder.email']}
        keyboardType="email-address"
        textContentType="emailAddress"
        defaultValue={form.email}
      />
      <TextInput
        onChangeText={text => handleOnChange(text, 'password')}
        placeholder={lang!['app.signin.placeholder.password']}
        textContentType="password"
        defaultValue={form.password}
        secureTextEntry
      />
      <Button
        title={lang!['app.buttons.signin']}
        btnStyles={formStyles.btnStyles}
        titleStyles={formStyles.titleStyles}
        onPress={() => handleSubmit(gotoProfile)}
      />
      <Text style={[formStyles.cta, formStyles.ctaText]}>
        {lang!['app.signin.no_account_yet']}
      </Text>
      <Text
        onPress={() => navigation.navigate(Screens.SignUp)}
        style={[formStyles.cta, formStyles.signUp]}
      >
        {lang!['app.signin.create_account']}
      </Text>
    </View>
  );
};

export default SignIn;
