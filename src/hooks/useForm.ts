import { useState } from 'react';
import { Alert } from 'react-native';

export default function useForm<T>(formData: T, asyncFunction: Function) {
  const [form, setForm] = useState(formData);

  const handleOnChange = (text: string, name: string) =>
    setForm((prevState: T) => ({ ...prevState, [name]: text }));

  const handleSubmit = async (nextAction?: Function) => {
    const response = await asyncFunction(form);

    if (!response.ok) return Alert.alert('Error', response.error);

    setForm(formData);
    if (nextAction) nextAction();
  };

  return { form, handleOnChange, handleSubmit };
}
