/*
 * https://reactnavigation.org/docs/stack-navigator/
 */
import React, { FC, useEffect, useState, useContext } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import { User } from 'firebase';

import WelcomeScreen from './src/screens/WelcomeScreen';
import SignIn from './src/screens/SignInScreen';
import SignUp from './src/screens/SignUpScreen';
import Profile from './src/screens/Profile';
import en from './src/common/translations/en.json';
import firebase from './src/api-sdk/firebase';
import AppContext from './src/contexts/AppContext';

const Stack = createStackNavigator();
const StackNavigator: FC = () => {
  const { user } = useContext(AppContext);

  return (
    <Stack.Navigator>
      <Stack.Screen
        name="XMentor"
        component={WelcomeScreen}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen name="Sign in" component={SignIn} />
      <Stack.Screen name="Sign up" component={SignUp} />
      {user && <Stack.Screen name="Profile" component={Profile} />}
    </Stack.Navigator>
  );
};

const App: FC = () => {
  const [user, setUser] = useState<User | null | undefined>();

  useEffect(() => {
    firebase.auth().onAuthStateChanged(user => {
      if (!user) return setUser(null);
      setUser(user);
    });
  }, []);

  return (
    <AppContext.Provider value={{ lang: en, user }}>
      <NavigationContainer>
        <StackNavigator />
      </NavigationContainer>
    </AppContext.Provider>
  );
};

export default App;
