/*
 *  https://callstack.github.io/react-native-testing-library/docs/api/
 *  https://reactnavigation.org/docs/testing
 */
import React, { FunctionComponent } from 'react';
import { render } from '@testing-library/react-native';

import AppContext from '../../../../src/contexts/AppContext';
import en from '../../../common/translations/en.json';
import { mockNavigation } from '../../../utils/mock-navigation';
import WelcomeComponent from '../';

const WelcomeScreen: FunctionComponent = () => {
  return (
    <AppContext.Provider value={{ lang: en, user: null }}>
      <WelcomeComponent navigation={mockNavigation} />
    </AppContext.Provider>
  );
};

describe('<WelcomeScreen />: ', () => {
  it('--- should renders correctly', () => {
    const container = render(<WelcomeScreen />).toJSON();
    expect(container).toMatchSnapshot();
  });

  it('--- should verify UI elements', () => {
    const { getAllByText } = render(<WelcomeScreen />);
    const signIn = getAllByText('Sign in');
    const signUp = getAllByText('Sign up');
    const xmentor = getAllByText('XMentor');

    expect(signIn).toHaveLength(1);
    expect(signUp).toHaveLength(1);
    expect(xmentor).toHaveLength(1);
  });
});
