import axios, { AxiosError, AxiosRequestConfig, AxiosResponse } from 'axios';

export const netClient = axios.create({
  baseURL: 'http://192.168.0.6:3000/api/v1',
  headers: {
    'Content-Type': 'application/json',
  },
});

const handleNetworkErrors = (error: AxiosError) =>
  Promise.reject({
    status: error.response?.status || 500,
    message:
      error.response?.data?.message ||
      error.message ||
      'A server error occurred.',
  });

netClient.interceptors.request.use(
  (config: AxiosRequestConfig) => config,
  handleNetworkErrors
);

netClient.interceptors.response.use(
  (response: AxiosResponse) => response,
  handleNetworkErrors
);
