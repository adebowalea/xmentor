/*
 * https://firebase.google.com/docs/web/setup#available-libraries
 */
import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

// Initialize Firebase
export default firebase.initializeApp({
  apiKey: 'AIzaSyAvScPjYUPyh9zIdFCM9JO75jN3tnTiHbY',
  authDomain: 'xmentor-ea064.firebaseapp.com',
  projectId: 'xmentor-ea064',
  storageBucket: 'xmentor-ea064.appspot.com',
  messagingSenderId: '29698834583',
  appId: '1:29698834583:web:f542b082237cab9a20a9d2',
});
