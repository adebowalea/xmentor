import React from 'react';
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  GestureResponderEvent,
} from 'react-native';

type ButtonProps = {
  title: string;
  onPress?: (_event: GestureResponderEvent) => void;
  btnStyles?: Record<string, string | number>;
  titleStyles?: Record<string, string | number>;
};

const Button: React.FC<ButtonProps> = ({
  title,
  onPress,
  btnStyles = {},
  titleStyles = {},
}: ButtonProps) => {
  return (
    <TouchableOpacity style={[styles.button, btnStyles]} onPress={onPress}>
      <Text style={[styles.text, titleStyles]}>{title}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  button: {
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 15,
    width: '100%',
    marginVertical: 10,
  },
  text: {
    fontSize: 17,
  },
});

export default Button;
