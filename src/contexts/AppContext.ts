import { createContext } from 'react';
import { User } from 'firebase';

type ContextProps = {
  lang: Record<string, string> | null;
  user: User | null | undefined;
};

const context: ContextProps = { lang: null, user: null };

const AppContext = createContext(context);

export default AppContext;
