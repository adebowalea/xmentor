import { Platform, StyleSheet } from 'react-native';

import colors from './colors';

export const defaultStyles = {
  colors,
  text: {
    color: colors.dark,
    fontSize: 18,
    fontFamily: Platform.OS === 'android' ? 'Roboto' : 'Avenir',
  },
};

export const formStyles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.lightGrey,
  },
  btnStyles: {
    width: '80%',
    backgroundColor: colors.pink,
  },
  titleStyles: {
    color: colors.white,
    fontWeight: 'bold',
  },
  cta: {
    fontSize: 16,
    color: colors.medium,
  },
  ctaText: {
    marginTop: 30,
  },
  signUp: {
    marginTop: 8,
    fontWeight: 'bold',
    textDecorationLine: 'underline',
  },
});
