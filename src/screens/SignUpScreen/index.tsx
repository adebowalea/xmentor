import React, { FunctionComponent, useContext } from 'react';
import { View, Text } from 'react-native';

import Button from '../../components/Button';
import { formStyles } from '../../common/styles';
import { NavigationProps, Screens } from '../../types/navigation-props';
import TextInput from '../../components/TextInput';
import AppContext from '../../../src/contexts/AppContext';
import useForm from '../../hooks/useForm';
import { api } from '../../api-sdk';
import { SignUpProps } from '../../types/forms';

const SignUp: FunctionComponent<NavigationProps> = ({ navigation }) => {
  const { lang } = useContext(AppContext);
  const { form, handleOnChange, handleSubmit } = useForm<Partial<SignUpProps>>(
    {},
    api.register
  );

  const gotoProfile = () => navigation.navigate(Screens.Profile);

  return (
    <View style={formStyles.container}>
      <TextInput
        onChangeText={text => handleOnChange(text, 'firstname')}
        placeholder={lang!['app.signin.placeholder.firstname']}
        defaultValue={form.firstname}
      />
      <TextInput
        onChangeText={text => handleOnChange(text, 'lastname')}
        placeholder={lang!['app.signin.placeholder.lastname']}
        defaultValue={form.lastname}
      />
      <TextInput
        onChangeText={text => handleOnChange(text, 'email')}
        placeholder={lang!['app.signin.placeholder.email']}
        keyboardType="email-address"
        textContentType="emailAddress"
        defaultValue={form.email}
      />
      <TextInput
        onChangeText={text => handleOnChange(text, 'password')}
        placeholder={lang!['app.signin.placeholder.password']}
        textContentType="password"
        defaultValue={form.password}
        secureTextEntry
      />
      <Button
        title={lang!['app.signin.create_account']}
        btnStyles={formStyles.btnStyles}
        titleStyles={formStyles.titleStyles}
        onPress={() => handleSubmit(gotoProfile)}
      />
      <Text style={[formStyles.cta, formStyles.ctaText]}>
        {lang!['app.signin.has_account']}
      </Text>
      <Text
        onPress={() => navigation.navigate(Screens.SignIn)}
        style={[formStyles.cta, formStyles.signUp]}
      >
        {lang!['app.buttons.signin']}
      </Text>
    </View>
  );
};

export default SignUp;
