import { netClient } from './net-client';
import firebase from './firebase';

import { LoginProps, SignUpProps, RequestProps } from '../types/forms';

const makeRequest = async (request: RequestProps, fn: Function) => {
  try {
    const response = await fn(request.url, request.data);
    return { ok: true, data: response.data };
  } catch (error) {
    return { ok: false, error };
  }
};

export const client = {
  get: (request: RequestProps) => {
    return makeRequest(request, netClient.get);
  },
  post: (request: RequestProps) => {
    return makeRequest(request, netClient.post);
  },
  put: (request: RequestProps) => {
    return makeRequest(request, netClient.put);
  },
  delete: (request: RequestProps) => {
    return makeRequest(request, netClient.delete);
  },
};

const getErrorMessage = (error: Record<string, string>) => {
  const fireMsg = error.message.split(': ');
  let errorMessage = 'An error occurred while creating your user account.';

  if (fireMsg.length === 1 && fireMsg[0].length) errorMessage = fireMsg[0];
  if (fireMsg.length === 2 && fireMsg[1].length) errorMessage = fireMsg[1];

  return { ok: false, error: errorMessage };
};

/*
 * https://firebase.google.com/docs/auth/web/password-auth
 */
export const api = {
  login: async (loginData: LoginProps) => {
    try {
      await firebase
        .auth()
        .signInWithEmailAndPassword(loginData.email, loginData.password);

      return {
        ok: true,
        message: 'Your login was successful.',
      };
    } catch (error) {
      return getErrorMessage(error);
    }
  },
  register: async (signUpData: LoginProps & SignUpProps) => {
    const successMessage = {
      ok: true,
      message: 'Your account was created successfully.',
    };

    try {
      await firebase
        .auth()
        .createUserWithEmailAndPassword(signUpData.email, signUpData.password);

      const { currentUser } = firebase.auth();

      if (currentUser) {
        const displayName = `${signUpData.firstname} ${signUpData.lastname}`;
        const photoURL = `https://example.com/${displayName
          .toLowerCase()
          .replace(' ', '_')}.jpg`;

        await currentUser.updateProfile({ displayName, photoURL });
      }

      return successMessage;
    } catch (error) {
      return getErrorMessage(error);
    }
  },
};
