export interface LoginProps {
  email: string;
  password: string;
}

export interface SignUpProps extends LoginProps {
  firstname: string;
  lastname: string;
}

export type RequestProps = {
  url: string;
  data: LoginProps | SignUpProps;
};
