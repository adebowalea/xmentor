import { StyleSheet } from 'react-native';
import colors from '../../common/colors';

export default StyleSheet.create({
  background: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.lightGrey,
  },
  buttonsContainer: {
    padding: 20,
    width: '100%',
  },
  logo: {
    width: 100,
    height: 100,
  },
  logoContainer: {
    position: 'absolute',
    top: 140,
    alignItems: 'center',
  },
  logoBg: {
    position: 'absolute',
    width: 150,
    height: 150,
    top: 80,
    padding: 5,
    opacity: 0.9,
    backgroundColor: colors.lightGray,
    borderRadius: 150,
    borderWidth: 10,
    borderColor: colors.pink,
  },
  tagline: {
    fontSize: 25,
    fontWeight: 'bold',
    color: colors.pink,
  },
  signIn: {
    borderWidth: 2,
    borderColor: '#dcdcde',
    backgroundColor: colors.lightGray,
  },
  signUp: {
    backgroundColor: colors.pink,
  },
  signInTitle: {
    color: colors.black,
  },
  signUpTitle: {
    color: colors.white,
  },
});
